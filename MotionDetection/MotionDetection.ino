#include <WiFi.h>
#include <WiFiUdp.h>
#include <coap.h>

/**************************************************************************************

This is a demo application to implement a motion detector using a PIR-sensor and CoAP.
The device will connect to an WLAN access point and use server with a static IP
Modify the credentials and IP for your own purpose!

coap://192.168.4.100/light

payload "1": switch off
payload "0": switch on

After a fixed timeout of 3 seconds the light will be switched off with a CoAP message.

***************************************************************************************/

//WPA2 credentials
const char* ssid     = "myssid";
const char* password = "mypassword";

volatile bool interruptDetected = false;
const byte interruptPin = 32;     /* pin 32 used for interrupt (other are possible) */

WiFiUDP udp;
Coap coap(udp);

/**************************************************************************************************************/

//interrupt service routine

void IRAM_ATTR handleInterrupt() {
  Serial.println("Interrupt Detected");
  interruptDetected = true;
}

/**************************************************************************************************************/

void setup(void) 
{ 
  // start serial port 
  Serial.begin(9600); 

  // set pin as input for interrupt
  pinMode(interruptPin, INPUT);
  attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt, RISING);

  // connect to wifi
  WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

  // print IP address
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // start coap client
  coap.start();
} 

void loop(void) 
{ 
  // in case of an interrupt, send coap PUT message to server 
  if(interruptDetected){
    delay(100);
   // coap.put(IPAddress(192,168,4,2), 5683, "motion", "1");    //it's possible to also send a message to a logging server
    coap.put(IPAddress(192,168,4,100), 5683, "light", "1");
   // coap.loop();
    delay(3000);
    coap.put(IPAddress(192,168,4,100), 5683, "light", "0");
    
    interruptDetected = false;
  }
  
} 
